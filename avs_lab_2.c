#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "argtable3.h"

struct arg_lit *help;
struct arg_dbl *a_arg, *b_arg, *h_arg, *eps_arg;
struct arg_end *end;

const double div_pi_by_4;

double y_func(double x);

double s_func(double y, double x, double eps, int32_t *n);

int main(int argc, char *argv[])
{
    void *argtable[] = {
        help = arg_litn(NULL, "help", 0, 1, "display this help and exit"),
        a_arg = arg_dbl1("a", NULL, "<n>", "a argument"),
        b_arg = arg_dbl1("b", NULL, "<n>", "b argument"),
        h_arg = arg_dbl1("h", NULL, "<n>", "h argument"),
        eps_arg = arg_dbl1("e", NULL, "<n>", "epsilon argument"),
        end = arg_end(5)
    };

    char progname[] = "avs_lab_2";

    int32_t nerrors = arg_parse(argc, argv, argtable);

    if (help->count > 0) {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout, argtable, "\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 0;
    }

    if (nerrors > 0) {
        arg_print_errors(stdout, end, progname);
        printf("Try \'%s --help\' for more information.\n", progname);
        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
        return 1;
    }

    if (nerrors == 0) {

        double a = *(a_arg->dval);
        double b = *(b_arg->dval);
        double h = *(h_arg->dval);
        double eps = *(eps_arg->dval);

        arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

        if (a >= b || h <= 0 || eps <= 0) {
            printf("Given arguments do not comply with the rules:\n"
                   "a < b\nh > 0\neps > 0\n");
            return 2;
        }

        __asm__ (
            "finit\n\t"
            "fldpi\n\t"         //st(0)=pi
            "fld1\n\t"          //st(0)=1, st(1)= pi
            "fadd st\n\t"       //st(0)=2, st(1)= pi
            "fadd st\n\t"       //st(0)=4, st(1)= pi
            "fdivp\n\t"         //st(0)=pi/4
            "fstp %[out]\n\t"
            :
            : [out] "m" (div_pi_by_4)
        );

        printf("\n%-15s|%-15s|%-15s|%-15s\n", "x", "Y(x)", "S(X)", "n");
        for (int i = 0; i < 63; ++i) {
            printf("=");
        }
        printf("\n");

        double x = a;

        while (x <= b) {
            double y = y_func(x);

            int32_t n = 0;

            double s = s_func(y, x, eps, &n);

            printf("%-15F|%-15F|%-15F|%-15d\n", x, y, s, n);
            x += h;
        }
    }

    return 0;
}

double y_func(double x)
{
    double y = 0;

    __asm__ volatile (
        ".section .data\n\t"
        "exp_is_neg: .byte 0\n\t"
        ".section .text\n\t"
        "finit\n\t"
        "fld %[div_pi_by_4]\n\t"
        "fcos\n\t"              //st(0)=cos(pi/4)=sin(pi/4)
        "fld %[x]\n\t"          //st(0)=x, st(1)=cos(pi/4)
        "fmulp\n\t"             //st(0)=x*cos(pi/4)
        "ftst\n\t"
        "fstsw ax\n\t"
        "sahf\n\t"
        "jae is_pos\n\t"
        "inc byte ptr[exp_is_neg]\n\t"
        "fabs\n\t"
    "is_pos:\n\t"
        "fldl2e\n\t"            //st(0)=l2e, st(1)=x*cos(pi/4)
        "fmul st(1)\n\t"        //st(0)=l2e*x*cos(pi/4), //st(1)=x*cos(pi/4)

        "fld st\n\t"            //st(0)=st(1)=l2e*x*cos(pi/4),
                                //st(2)=x*cos(pi/4)
 
        "frndint\n\t"           //st(0)=rnd(l2e*x*cos(pi/4)),
                                //st(1)=l2e*x*cos(pi/4), //st(2)=x*cos(pi/4)

        "fsub st(1), st\n\t"    //st(0)=rnd(l2e*x*cos(pi/4)),
                                //st(1)=l2e*x*cos(pi/4)-rnd(l2e*x*cos(pi/4)),
                                //st(2)=x*cos(pi/4)

        "fxch\n\t"
        "f2xm1\n\t"
        "fld1\n\t"
        "faddp\n\t"
        "fscale\n\t"
        "fxch\n\t"
        "ffree st\n\t"
        "fincstp\n\t"
        "cmp byte ptr[exp_is_neg], 1\n\t"
        "jne exp_ready\n\t"
        "fld1\n\t"
        "fxch\n\t"
        "fdivp\n\t"
    "exp_ready:\n\t"
        "fxch\n\t"
        "fcos\n\t"              //st(0)=cos(x*cos(pi/4)),
                                //st(1)=e^(x*cos(pi/4))
        "fmulp\n\t"             //st(0)=Y(x)
        "fstp %[y]\n\t"         //y=st(0)
        : [y] "=m" (y)
        : [x] "m" (x), [div_pi_by_4] "m" (div_pi_by_4)
    );

    return y;
}

double s_func(double y, double x, double eps, int32_t *n)
{
    double s = 0;

    int32_t k = 0;

    __asm__ (
        "finit\n\t"
        "fld1\n\t"              //k!
        "fldz\n\t"              //a
        "fld %[x]\n\t"          //x
        "ftst\n\t"
        "fstsw ax\n\t"
        "sahf\n\t"
        "je exit\n\t"
        "fldz\n\t"              //k
        "fldz\n\t"              //S(x)
        "jmp iteration\n\t"
    "pre_iteration:\n\t"
        "fxch\n\t"
        "fld1\n\t"
        "faddp\n\t"
        "fmul st(4), st\n\t"
        "fxch\n\t"
        "fxch st(3)\n\t"
        "fld %[div_pi_by_4]\n\t"
        "faddp\n\t"
        "fxch st(3)\n\t"
    "iteration:\n\t"
        "fld st(1)\n\t"         //st(0)=k
        "fld st(3)\n\t"         //st(0)=x, st(1)=k
        "fyl2x\n\t"             //st(0)=k*l2x
        "fld st\n\t"            //st(0)=st(1)=k*l2x
        "frndint\n\t"
        "fsub st(1), st\n\t"
        "fxch\n\t"
        "f2xm1\n\t"
        "fld1\n\t"
        "faddp\n\t"
        "fscale\n\t"
        "fxch\n\t"
        "ffree st\n\t"
        "fincstp\n\t"           //st(0)=x^k
        "fld st(4)\n\t"
        "fcos\n\t"
        "fmulp\n\t"             //st(0)=cos(a)*x^4
        "fdiv st(5)\n\t"        //st(0)=cos(a)*x^4/k!
        "faddp\n\t"             //st(0)=S(x)
        "fld %[y]\n\t"
        "fsub st, st(1)\n\t"
        "fabs\n\t"
        "fld %[eps]\n\t"
        "fcompp\n\t"            //eps ? |Y(x)-S(x)|
        "fstsw ax\n\t"
        "sahf\n\t"
        "jb pre_iteration\n\t"  //eps < |Y(x)-S(x)|
        "fstp %[s]\n\t"
        "fistp %[k]\n\t"
    "exit:\n\t"
        : [s] "=m" (s), [k] "=m" (k)
        : [y] "m" (y), [x] "m" (x), [eps] "m" (eps),
            [div_pi_by_4] "m" (div_pi_by_4)
    );

    *n = k;
    return s;
}
