Лабораторная работа №2
======================

Значение аргумента x изменяется от a до b с шагом h. Для каждого x найти 
значения функции Y(x), суммы S(x) и число итераций n, при котором достигается 
требуемая точность ε = |Y(x)-S(x)|. Результат вывести в виде таблицы. Значения 
a, b, h и ε вводятся с клавиатуры.
Работу программы проверить для a=0,1; b=0,8; h=0,1.

Вариант 3
---------

![equation](http://www.sciweavers.org/tex2img.php?eq=S%28x%29%20%3D%20%5Csum_%7Bk%3D0%7D%5E%7Bn%7D%20%5Cfrac%7B%5Ccos%20%28%5Cfrac%7Bk%5Cpi%7D%7B4%7D%29%7D%7Bk%21%7D%20x%5E%7Bk%7D%2C%20%5C%5C%0AY%28x%29%20%3D%20e%5E%7Bx%20%5Ccos%5Cfrac%7B%5Cpi%7D%7B4%7D%7D%5Ccos%28x%5Csin%5Cfrac%7B%5Cpi%7D%7B4%7D%29.&bc=White&fc=Black&im=jpg&fs=12&ff=arev&edit=0)
